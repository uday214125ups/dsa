#include <stdio.h>
#include <stdlib.h>
struct node
{
  int data;
  struct node *link;
} * start;

void createList(int n)

{
  struct node *new, *ptr, *p, *c;
  int data;

  start = (struct node *)malloc(sizeof(struct node));

  printf("enter the data of first node:-");

  scanf("%d", &data);
  start->data = data;
  start->link = NULL;
  ptr = start;
  printf("%d", data);
  for (int i = 2; i <= n; i++)
  {
    new = (struct node *)malloc(sizeof(struct node));

    printf("enter data in nodes");
    scanf("%d", &data);

    new->data = data;
    new->link = NULL;
    ptr->link = new;
    ptr = ptr->link;
  }

  if (ptr->link == NULL)
  {
    ptr->link = start;
  }
}

void traverseList()
{
  struct node *ptr;

  ptr = start;
  while (ptr->link != start)
  {
    printf("Data = %d\n", ptr->data);
    ptr = ptr->link;
  }

  printf("Data = %d\n", ptr->data);
}

void updatelist()
{

  int a;

  printf("enter the position where you want to insert data");

  scanf("%d", &a);

  struct node *new, *ptr;
  int data;

  new = (struct node *)malloc(sizeof(struct node));

  printf("enter the data");
  scanf("%d", &data);
  new->data = data;
  new->link = NULL;

  ptr = start;

  if (a == 1)
  {
    new->link = start;
    start = new;
  }
  if (a != 1)
  {

    for (int i = 1; i < a - 1; i++)
    {
      ptr = ptr->link;
    }
    new->link = ptr->link;
    ptr->link = new;
  }

  

  while (ptr->link != start)
  {

    ptr = ptr->link;
  }

  if (ptr->link == NULL)
  {
    ptr->link = start;
  }
}
int main()
{
  int n;
  printf("enter the no of nodes-");
  scanf("%d", &n);
  createList(n);
  updatelist();
  traverseList();
}
